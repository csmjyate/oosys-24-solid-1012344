package ljmu.vets;

/**
 * This is the enum for the file type, this allows us to add more file types in
 * the future and declares all of the file types used in one space keeping the
 * code more maintainable and readable.
 * 
 * @author jacky
 *
 */
public enum FileType {
	DOCX, XPS, PDF;
}
