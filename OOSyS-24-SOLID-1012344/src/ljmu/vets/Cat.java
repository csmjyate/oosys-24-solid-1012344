package ljmu.vets;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
/**
 * The cat class is an extension of the pet class, therefore carries over the same characteristics.
 * @author jacky
 *
 */
public class Cat extends Pet {

	
	private Breeding breeding;

	/**
	 * The parameters make up the characteristics of a cat and the enum breeding is
	 * utilised here with the selection of many different cat breeds
	 * 
	 * @param name
	 * @param regDate
	 * @param breeding
	 */
	public Cat(String name, LocalDate regDate, Breeding breeding) {
		super(name, regDate);

		this.breeding = breeding;
	}
/**
 * For outputting all of the cat details to a string.
 */
	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " >> " +
				this.name + " " +
				this.regDate.format(DateTimeFormatter.ofPattern("dd-MMM-yy")) + " " +
				this.breeding.toString();
	}
	
	/**
	 * The calculate cost method here is the example of the cost of a booking for a
	 * cat, this links to the booking class.
	 */
	 @Override
	    public double calculateCost(double duration) {
	        return duration * 13.0; 
	    }

	// ToDo : get / set Methods ?
	/**
	 * The get and set methods are to retrieve and manipulate the characterisitcs of
	 * the cat species.
	 * 
	 * @return
	 */
	public Breeding getBreeding() {
		return breeding;
	}

	public void setBreeding(Breeding breeding) {
		this.breeding = breeding;
	}
}
