package ljmu.vets;

public class SupplyHub implements Notifiable {
	private String name;

	/**
	 * The supply hub is used to let the staff know about the medicine count and its
	 * supplies
	 * 
	 * @param n
	 */
	public SupplyHub(String n) {
		this.name = n;
	}

	@Override
	public void notifyy(String s) {
		System.out.println(this.name + " KNOWS ABOUT " + s);
	}
}
