package ljmu.vets;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Class to calculate the invoice of a booking at the veterinary.
 * 
 * @author jacky
 *
 */
public class InvoiceCalculator {

	/**
	 * To calculate the amount of an invoice for a booking we need to go into the
	 * bookings class and array of data. Also need to go into pets to find the
	 * species as they have varying prices.
	 * 
	 * @param bookings
	 * @return
	 */
	public Double calculateAmount(List<Booking> bookings) {
		Double totalAmount = 0.0;

		for (Booking booking : bookings) {
			Pet pet = booking.getPet();
			double duration = booking.getDuration();

			totalAmount += pet.calculateCost(duration);
		}
		return Double.parseDouble(new DecimalFormat("#.##").format(totalAmount));
	}
}
