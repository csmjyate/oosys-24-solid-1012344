package ljmu.vets;

/**
 * This is the class for the species fish with its uniques characteristic to
 * other animals of having water types.
 * 
 * @author jacky
 *
 */
public enum WaterType {
	SALT, FRESH;
}
