package ljmu.vets;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * The booking class sets the properties for when a booking is made. The booking
 * properties are calculated from other classes on the system and this class
 * simply is the characteristics made up with the getters and setters.
 * 
 * @author jacky
 *
 */
public class Booking implements Serializable {
	private String ref;
	private Pet pet;
	private LocalDateTime when;
	private Integer duration;

	/**
	 * THe parameters are the characteristics to make up the bookings. These link to the pets class and invoice calculator.
	 * @param ref
	 * @param pet
	 * @param when
	 * @param duration
	 */
	public Booking(String ref, Pet pet, LocalDateTime when, Integer duration) {
		this.ref = ref;
		this.pet = pet;
		this.when = when;
		this.duration = duration;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " >> " + this.ref + " " + this.pet.toString() + " "
				+ this.when.format(DateTimeFormatter.ofPattern("dd-MMM-yy HH:mm"));
	}

	public String getRef() {
		return this.ref;
	}

	public Pet getPet() {
		return this.pet;
	}

	public LocalDateTime getWhen() {
		return this.when;
	}

	public Integer getDuration() {
		return this.duration;
	}

	// ToDo : get / set Methods ?
	public void setRef(String ref) {
		this.ref = ref;
	}

	public void setPet(Pet pet) {
		this.pet = pet;
	}

	public void setWhen(LocalDateTime when) {
		this.when = when;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

}
