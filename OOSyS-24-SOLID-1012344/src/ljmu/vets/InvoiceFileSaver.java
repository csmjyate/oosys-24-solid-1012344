package ljmu.vets;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.File;

/**
 * Used to save an invoice statement in a form of file type based on users selection.
 * @author jacky
 *
 */
public class InvoiceFileSaver {
//	Under Development
	private void saveAsDocX(String path) {

	}

	public void saveAsXPS(String path) {

	}

	public void saveAsPDF(String path) {

	}

	public void saveAs(FileType fileType, Invoice invoice, String path) {
		switch (fileType) {
		case DOCX: {
			saveAsDocX(path);
			break;
		}
		case XPS: {
			saveAsXPS(path);
			break;
		}
		case PDF: {
			saveAsPDF(path);
			break;
		}
		}
	}
}

