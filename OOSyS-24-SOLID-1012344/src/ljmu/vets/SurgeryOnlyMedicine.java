package ljmu.vets;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This is the class which handles only the medicine which is available for the surgery.
 * @author jacky
 *
 */
public class SurgeryOnlyMedicine implements Serializable {
	
	protected String name;
	protected Integer stock, lowest;
	protected Double surgeryCost;
	protected List<Notifiable> people = new ArrayList<Notifiable>();

	/**
	 * The characteristics of the medicine available to the surgery.
	 * @param name
	 * @param stock
	 * @param lowest
	 * @param surgeryCost
	 */
	public SurgeryOnlyMedicine(String name, Integer stock, Integer lowest, Double surgeryCost) {
		this.name = name;
		this.stock = stock;
		this.lowest = lowest;
		this.surgeryCost = surgeryCost;
	}
	
	public void addNotifiable(Notifiable n) {
		this.people.add(n);
	}

	/**
	 * The get and set methods here are used to retriece and manipulate the data for
	 * the medicine only available to the surgery
	 */
	// ToDo : get / set Methods ?
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getLowest() {
		return lowest;
	}

	public void setLowest(Integer lowest) {
		this.lowest = lowest;
	}
/**
 * Used to notify people at the surgery which needs notifying about the medicine.
 * @return
 */
	public List<Notifiable> getPeople() {
		return people;
	}

	/**
	 * Used to set the people which need notifying about the medicine.
	 * @param people
	 */
	public void setPeople(List<Notifiable> people) {
		this.people = people;
	}

	public Integer getStock() {
		return stock;
	}

	public void setSurgeryCost(Double surgeryCost) {
		this.surgeryCost = surgeryCost;
	}

	public Double getPublicCost() throws Exception {
			throw new Exception("ToDo");
	}

	public void setStock(Integer stockLevel) {
		
	}
//	private void notifyPeople() {
//	for	(Notifiable n : people) {
//		n.notifyy(this.stock + " LEFT OF " + this.name);
//	}
//}	

//@Override
//public Double getSurgeryCost() {
//	return this.surgeryCost;
//}
//
//@Override
//public Double getPublicCost() throws Exception {
//	throw new Exception("ToDo");
//}
	
//	public void setStock(Integer i) {
//	this.stock = i;
//
//	if (i < lowest) {
//		notifyPeople();
//	}
//}

}
