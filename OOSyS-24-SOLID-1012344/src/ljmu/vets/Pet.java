package ljmu.vets;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * The pets class holds and links all of the information to do with pets in the vets system.
 * @author jacky
 *
 */
public abstract class Pet implements Serializable {
    public abstract double calculateCost(double duration);
	protected String name;
	protected LocalDate regDate;
	

	// ToDo : Private ?
	/**
	 * Links the pets to the bookings by introducing the array of all the bookings,
	 * which includes the information on pets.
	 */
//	public List<Booking> bookings = new ArrayList<Booking>();
	private List<Booking> bookings = new ArrayList<Booking>();
	
	public Pet(String name, LocalDate regDate) {
		this.name = name;
		this.regDate = regDate;
	}

/**
 * gets the next booking of a pet.
 * @return
 */
	public Booking getNextBooking() {
		return this.bookings.stream()
			.filter(o -> o.getWhen().isAfter(LocalDateTime.now()))
				.sorted(Comparator.comparing(o -> o.getWhen()))
					.findFirst().get();
	}
	
	public String getName() {
		return this.name;
	}

	public LocalDate getRegDate() {
		return this.regDate;
	}

	// ToDo : get / set Methods ?
	/**
	 * Retrieves all of the bookings as all of the bookings include a pet.
	 * @return
	 */
	public List<Booking> getBookings() {
		return bookings;
	}

	public void setBookings(List<Booking> bookings) {
		this.bookings = bookings;
	}

	/**
	 * The getters and setters are to recieve and manipulate the general data
	 * surrounding pets but not the unique information such as breeds and water
	 * type.
	 */
	public void setName(String name) {
		this.name = name;
	}

	public void setRegDate(LocalDate regDate) {
		this.regDate = regDate;
	}

	public void makeBooking(Booking when) {
		this.bookings.add(when);
	}
}
