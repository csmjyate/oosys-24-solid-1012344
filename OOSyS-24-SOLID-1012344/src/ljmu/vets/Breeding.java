package ljmu.vets;

/**
 * This is the enum for the pets class, breeding and defines the type of breed
 * for the cat species.
 * 
 * @author jacky
 *
 */
public enum Breeding {
	MOGGIE, PEDIGREE;
}
