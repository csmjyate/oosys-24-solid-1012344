package ljmu.vets;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * The fish class extends the pet class as it is one of the species of pets.
 * Similar to cats this has a water type characteristic whch is unique to the
 * species of animal. Therefore is used in the extension.
 * 
 * @author jacky
 *
 */
public class Fish extends Pet {

	/**
	 * THe calculation for the invoice of a booking at the surgery for a fish at the vets
	 */
	@Override
	public double calculateCost(double duration) {
		return duration * 17.0; 
	}
	
	private WaterType waterType;

	/**
	 * The parameters make up the characteristics of the fish sharing some with the
	 * pets class, however is an extension with the unique parameter of water type.
	 * 
	 * @param name
	 * @param regDate
	 * @param waterType
	 */
	public Fish(String name, LocalDate regDate, WaterType waterType) {
		super(name, regDate);

		this.waterType = waterType;
	}

	/**
	 * Used to output the characteristics of the fish to a string format.
	 */
	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " >> " +
				this.name + " " +
				this.regDate.format(DateTimeFormatter.ofPattern("dd MMM yy")) + " " +
				this.waterType.toString();
	}

	
	/**
	 * The get and set methods are to retrieve the characteristic of the fish here.
	 * This is used for the retrieval and manipulation of data about the fish.
	 * 
	 * @param waterType
	 */
	// ToDo : get / set Methods ?
	public void setWaterType(WaterType waterType) {
		this.waterType = waterType;
	}
	
	public WaterType getWaterType() {
		return waterType;
	}
}
