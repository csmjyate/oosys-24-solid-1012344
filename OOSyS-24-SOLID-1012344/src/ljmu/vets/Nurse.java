package ljmu.vets;

public class Nurse implements Notifiable {
	private String name;

	public Nurse(String n) {
		this.name = n;
	}

	/**
	 * Used to notify the system that a nurse knows about the stock level/
	 */
	@Override
	public void notifyy(String s) {
		System.out.println(this.name + " KNOWS ABOUT " + s);
	}
}
