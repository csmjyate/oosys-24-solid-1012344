package ljmu.vets;

/**
 * 
 * @author jacky
 *Payable interface handles anything to do with the cost of anything.
 */
public interface Payable {
	public Double getSurgeryCost();
	public Double getPublicCost() throws Exception;
}
