package ljmu.vets;

/**
 * Interface used to notify the user on the system.
 * @author jacky
 *
 */
public interface Notifiable {
	public void notifyy(String s);
}
