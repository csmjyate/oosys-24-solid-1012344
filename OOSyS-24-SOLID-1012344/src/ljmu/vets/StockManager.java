package ljmu.vets;

import java.util.ArrayList;
import java.util.List;

/**
 * Stock manager handles the systems medicine availabilities.
 * @author jacky
 *
 */
public class StockManager {
    private List<SurgeryOnlyMedicine> medicines = new ArrayList<>();

/**
 * Notifies people the amount of stock left in a medicine count.
 * @param medicine
 */
    private void notifyPeople(SurgeryOnlyMedicine medicine) {
        for (Notifiable person : medicine.getPeople()) {
            person.notifyy(medicine.getStock() + " LEFT OF " + medicine.getName());
        }
    }
    /**
     * To add a medicine to the stock.
     * @param medicine
     */
    public void addMedicine(SurgeryOnlyMedicine medicine) {
        medicines.add(medicine);
    }

  /**
   * To remove a medicine from the vets.
   * @param medicine
   */
    public void removeMedicine(SurgeryOnlyMedicine medicine) {
        medicines.remove(medicine);
    }

    /**
     * Used to set the stock level of a certain medicine.
     * @param medicine
     * @param stockLevel
     */
    public void setStockLevel(SurgeryOnlyMedicine medicine, Integer stockLevel) {
        medicine.setStock(stockLevel);
        if (stockLevel < medicine.getLowest()) {
            notifyPeople(medicine);
        }
    }
/**
 * Used to notify a person about the medicine.
 * @param medicine
 * @param person
 */
    
    public void addNotifiableToMedicine(SurgeryOnlyMedicine medicine, Notifiable person) {
        medicine.addNotifiable(person);
    }
}
