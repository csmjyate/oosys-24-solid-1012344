package ljmu.vets;

/**
 * This class is for the characteristics of medicine which is only available to the public.
 * @author jacky
 *
 */
public class PublicMedicine extends SurgeryOnlyMedicine {

    private Double publicCost;

/**
 * The characteristics include the stock level of the public medicine available
 * at the vets for sale.
 * 
 * @param name
 * @param stock
 * @param lowest
 * @param surgeryCost
 * @param publicCost
 */
    public PublicMedicine(String name, Integer stock, Integer lowest, Double surgeryCost, Double publicCost) {
        super(name, stock, lowest, surgeryCost);
        this.publicCost = publicCost;
    }

    /**
     * Sets the stock level of the medicine in availability.
     */
    @Override
    public void setStock(Integer i) {
        super.setStock(i); 
    }
/**
 * Gets and sets the cost for the public on the medicine.
 */
    @Override
    public Double getPublicCost() {
        return this.publicCost;
    }
/**
 */
    public void setPublicCost(Double publicCost) {
        this.publicCost = publicCost;
    }
}
