package ljmu.vets;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.List;

/**
 * The invoice class is used for the details of an invoice for a booking at the
 * veterinary
 * 
 * @author jacky
 *
 */
public class Invoice implements Serializable {
	private Integer no;
	private LocalDateTime when;
	private Double amount;
	private List<Booking> bookings;
	private List<Payable> payables;

/**
 * The invoice class holds many attributes which relate to the calculation of an
 * invoice for a booking and therefore needs to implement the bookings array and
 * payables array
 * 
 * @param no
 * @param when
 * @param amount
 * @param bookings
 * @param payables
 */
	public Invoice(Integer no, LocalDateTime when, Double amount, List<Booking> bookings, List<Payable> payables) {
		this.no = no;
		this.when = when;
		this.amount = amount;
		
		InvoiceCalculator calculator = new InvoiceCalculator();
        this.amount = calculator.calculateAmount(bookings);

		this.bookings = bookings;
		this.payables = payables;
	}


	// ToDo : get / set Methods ?
	/**
	 * The get and set methods in this class are used to retrieve the
	 * characteristics of the invoice for retrieval of information about an invoice
	 * or a manipulation of data avout an invoice as well.
	 * 
	 * @return
	 */
	public Integer getNo() {
		return no;
	}

	public void setNo(Integer no) {
		this.no = no;
	}

	public LocalDateTime getWhen() {
		return when;
	}

	public void setWhen(LocalDateTime when) {
		this.when = when;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	/**
	 * Used to get the array of bookings to link the classes together and therefore calculate an invoice.
	 * @return
	 */
	public List<Booking> getBookings() {
		return bookings;
	}
/**
 * Used to manipulate a booking based on an invoice of that booking
 * @param bookings
 */
	public void setBookings(List<Booking> bookings) {
		this.bookings = bookings;
	}

	/**
	 * Used to get the array of information about the payables to do with an invoice.
	 * @return
	 */
	public List<Payable> getPayables() {
		return payables;
	}

	/**
	 * Used to change a payable to do with the invoice in quesiton
	 * @param payables
	 */
	public void setPayables(List<Payable> payables) {
		this.payables = payables;
	}

}
