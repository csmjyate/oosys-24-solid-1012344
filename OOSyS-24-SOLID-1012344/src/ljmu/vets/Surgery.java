package ljmu.vets;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * The surgery class handles all of the information regarding the surgery and its characteristics.
 * @author jacky
 *
 */
public class Surgery implements Serializable {
	private String surgery;
	private List<Pet> pets = new ArrayList<Pet>();
	private List<Booking> bookings = Collections.synchronizedList(new ArrayList<Booking>());

	public Surgery(String surgery) {
		this.surgery = surgery;
	}

	/** 
	 * Prints the name of the surgery we are using.
	 */
	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " >> " + this.surgery;
	}

	// ToDo : get / set Methods ?

	/**
	 * Adds a pet to the surgery.
	 * @param pet
	 */
	public void makePet(Pet pet) {
		// ToDo : Validate ?
		this.pets.add(pet);
	}

	
	public void makePet(String name, LocalDate regDate) {
		// ToDo : Validate ?
	}
	
/**
 * Gets a pet by name at the surgery.
 * @param name
 * @return
 */
	public Pet getPetByName(String name) {
		// NOTE : Java SE 7 Code !
		for (Pet p : this.pets) {
			if (p.getName().equals(name)) {
				return p;
			}
		}

////		 NOTE : Java SE 8 Code !
//		Optional<Pet> p = this.pets.stream().filter(o -> o.getName().equals(name)).findAny();
//		if (p.isPresent()) {
//			return p.get();
//		}
//		

		// NOTE : No Match !
		return null;
	}

	/**
	 * Makes a booking for that surgery.
	 * @param booking
	 */
	public  void makeBooking(Booking booking) {
		// ToDo : Validate ?
		this.bookings.add(booking);

		// ToDo Two-Way Linking ?
		booking.getPet().makeBooking(booking);
	}

	
	public void makeBooking(String ref, Pet pet, LocalDateTime when) {
		// ToDo : Validate ?
	}
	

//	ToDo : getBookingByRef() ?
	public void getBookingByRef(Booking booking) {

	}
}
