package main;

import java.util.Scanner;

import ljmu.vets.Surgery;

/**
 * Interface to handle the booking commands. Had a try at making a booking menu for interface segregation principle.
 * @author jacky
 *
 */
public interface BookingCommand {
    void execute(Surgery surgery, Scanner scanner);
}

