package main;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

import ljmu.vets.Booking;
import ljmu.vets.Pet;
import ljmu.vets.Surgery;

/**
 * The class to Make a booking for a pet at the surgery.
 * @author jacky
 *
 */
public class MakeBooking implements Command {
    private final Surgery surgery;
	private final Scanner S = new Scanner(System.in);

    public MakeBooking(Surgery surgery, Scanner scanner) {
        this.surgery = surgery;
    }

    /**
     * Executes the main logic to make a booking for the surgery in question.
     */
    @Override
    public void execute() {
        System.out.print("Booking's Ref : ");
        String ref = S.nextLine();

        System.out.print("Pet's Name : ");
        String name = S.nextLine();

        // ToDo: Validate ?
        Pet pet = surgery.getPetByName(name);

        if (pet == null) {
            System.out.println("Pet not found. Please try again or check the spelling.");
            return; 
        }

        System.out.print("Booking's DateTime [i.e. 03 OCT 23 09:30] : ");
        LocalDateTime when = LocalDateTime.parse(S.nextLine(), DateTimeFormatter.ofPattern("dd MMM yy HH:mm"));

        System.out.print("Booking's Duration [i.e. MINS] : ");
        Integer duration = Integer.parseInt(S.nextLine());

        surgery.makeBooking(new Booking(ref, pet, when, duration));
        Booking booking = new Booking(ref, pet, when, duration); 
        surgery.makeBooking(booking);
        System.out.println("Booking created successfully:");
        System.out.println(booking.toString()); 
    }

}
