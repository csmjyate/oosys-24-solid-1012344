package main;

/**
 * Command interface for the command factory to fix the open closed principle/
 * @author jacky
 *
 */
public interface Command {
    void execute();
}

