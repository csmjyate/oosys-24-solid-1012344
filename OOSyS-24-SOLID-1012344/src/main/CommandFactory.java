package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import ljmu.vets.Surgery;

public class CommandFactory {
    private final List<Class<? extends Command>> commandClasses;

	/**
	 * The command factory utilised commands in an array to extend on the
	 * possibilities of more features being added without modifying we can just
	 * extend.
	 */
	public CommandFactory() {
        commandClasses = new ArrayList<>();
        commandClasses.add(SurgeryMenu.class); //1
        commandClasses.add(MakePet.class); //2
        commandClasses.add(GetPetByName.class); //3
        commandClasses.add(MakeBooking.class);  //4
        commandClasses.add(GetPetsNextBooking.class); //5
    }

	/**
	 * We can extend on more features to the veterinary system for each surgery. 
	 * @param commandType
	 * @param surgery
	 * @param scanner
	 * @return
	 */
    public Command createCommand(String commandType, Surgery surgery, Scanner scanner) {
        try {
            int index = Integer.parseInt(commandType) - 1; 
            if (index < 0 || index >= commandClasses.size()) {
                throw new IllegalArgumentException("Invalid command type");
            }

            Class<? extends Command> commandClass = commandClasses.get(index);
            return commandClass.getDeclaredConstructor(Surgery.class, Scanner.class)
                               .newInstance(surgery, scanner);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Invalid command type: " + commandType, e);
        } catch (Exception e) {
            throw new RuntimeException("Failed to create command", e);
        }
    }

/**
 * Used to retrieve a command from the array of command classes.
 * @param choice
 * @param surgery
 * @param scanner
 * @return
 */
    public Command getCommand(String choice, Surgery surgery, Scanner scanner) {
        int index;
        try {
            index = Integer.parseInt(choice) - 1;
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Invalid command type: " + choice);
        }

        if (index >= 0 && index < commandClasses.size()) {
            try {
                Class<? extends Command> commandClass = commandClasses.get(index);
                return commandClass.getDeclaredConstructor(Surgery.class, Scanner.class)
                                   .newInstance(surgery, scanner);
            } catch (Exception e) {
                throw new RuntimeException("Failed to get command", e);
            }
        } else {
            throw new IllegalArgumentException("Invalid command type");
        }
    }


	/**
	 * To add a command to the array of command classes
	 * 
	 * @param commandClass
	 */
	public void addCommand(Class<? extends Command> commandClass) {
		commandClasses.add(commandClass);
	}

	/**
	 * To remove a command from the array of command classes
	 * 
	 * @param commandClass
	 */
	public void removeCommand(Class<? extends Command> commandClass) {
		commandClasses.remove(commandClass);
	}
}


