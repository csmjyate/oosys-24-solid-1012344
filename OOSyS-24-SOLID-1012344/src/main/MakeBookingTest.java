package main;

import static org.junit.Assert.assertTrue;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ljmu.vets.Surgery;

/**
 * Unit test for making a booking for the veterinary system.
 * 
 * @author jacky
 *
 */
public class MakeBookingTest {
	private MakeBooking makeBooking;
	private Surgery surgery;
	private ByteArrayOutputStream output;

	/**
	 * Sets up the environment for the input of data to see whether the
	 * functionality of the method works.
	 */
	@Before
	public void setUp() {
		surgery = new Surgery("Test Surgery");
		makeBooking = new MakeBooking(surgery, new Scanner(System.in));

		output = new ByteArrayOutputStream();
		System.setOut(new PrintStream(output));
	}

	/**
	 * Sets the modifications made as null so the test doesn't effect the system at
	 * all.
	 */
	@After
	public void tearDown() {
		surgery = null;
		makeBooking = null;
		output = null;
	}

	/**
	 * Performs the unit test for making a booking at the surgery using data being
	 * inputted.
	 */
	@Test
	public void testExecute_BookingSuccess() {

		String input = "123\nGlynH\n03 Oct 23 09:30\n20\n";
		InputStream in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);

		makeBooking.execute();

		assertTrue(output.toString().contains("Booking created successfully"));
	}

}



