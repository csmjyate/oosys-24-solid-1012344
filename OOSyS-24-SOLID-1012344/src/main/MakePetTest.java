package main;

import ljmu.vets.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/** 
 * Unit test for the making of a pet at the surgery functionality.
 * @author jacky
 *
 */
public class MakePetTest {
    private MakePet makePet;
    private Surgery surgery;
    private ByteArrayOutputStream output;

    /**
     * Setting up the environment for a test run of data being inputted for the unit test.
     */
    @Before
    public void setUp() {
        surgery = new Surgery("Test Surgery");
        output = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(output);
        Scanner scanner = new Scanner(System.in);
        makePet = new MakePet(surgery, scanner);
        System.setOut(printStream);
    }

    /**
     * Sets all to null to not modify the system at all.
     */
    @After
    public void tearDown() {
        surgery = null;
        makePet = null;
        output = null;
    }
    /**
     * Unit test for making a pet, I chose to do in the case of making a fish
     */
    @Test
    public void testExecute_MakeFish() {
   
        String input = "Nemo\n03 Oct 23\n2\n1\n";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

    
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(outputStream);
        System.setOut(printStream);

    
        Scanner scanner = new Scanner(System.in);

    
        makePet = new MakePet(surgery, scanner);

     
        makePet.execute();

      
        assertTrue(outputStream.toString().contains("Select the water type for the fish:"));
        assertTrue(outputStream.toString().contains("1. " + WaterType.SALT));
        assertTrue(outputStream.toString().contains("2. " + WaterType.FRESH));
        assertTrue(outputStream.toString().contains("Enter your choice:"));
        assertTrue(outputStream.toString().contains("Pet Added Successfully!"));
        
        
        Pet pet = surgery.getPetByName("Nemo");
        assertEquals("Nemo", pet.getName());
        
      
        assertTrue(pet instanceof Fish);
        
     
        assertEquals(WaterType.SALT, ((Fish) pet).getWaterType());
        
   
        LocalDate expectedRegDate = LocalDate.parse("03 Oct 23", DateTimeFormatter.ofPattern("dd MMM yy"));
        assertEquals(expectedRegDate, pet.getRegDate());
    }
}


