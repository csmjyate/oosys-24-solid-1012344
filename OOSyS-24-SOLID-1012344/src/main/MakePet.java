package main;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

import ljmu.vets.*;

/**
 * Class used to make a pet at the surgery.
 * @author jacky
 *
 */
public class MakePet implements Command {
    private final Surgery surgery;
    private final Scanner scanner;

    /**
     * Scanner to allow the user to make a pet and Surgery to assign the pet the surgery it is being created.
     * @param surgery
     * @param scanner
     */
    public MakePet(Surgery surgery, Scanner scanner) {
        this.surgery = surgery;
        this.scanner = scanner;
    }

    /**
     * Logic to execute the making of a pet at the surgery.
     */
    @Override
    public void execute() {
        System.out.print("Pet's Name: ");
        String name = scanner.nextLine();

        System.out.print("Pet's RegDate [i.e. 03 OCT 23]: ");
        LocalDate regDate = LocalDate.parse(scanner.nextLine(), DateTimeFormatter.ofPattern("dd MMM yy"));

        System.out.println("Select the type of pet:");
        System.out.println("1. Cat");
        System.out.println("2. Fish");
        System.out.print("Enter your choice: ");
        int choice = Integer.parseInt(scanner.nextLine());

        switch (choice) {
            case 1:
                makeCat(name, regDate);
                break;
            case 2:
                makeFish(name, regDate);
                break;
            default:
                System.out.println("Invalid choice. Please try again.");
        }
    }

    /**
     * Parameters passed as they have already been inputted. here is the menu to select the characteristics if it is an instance of a cat.
     * @param name
     * @param regDate
     */
    private void makeCat(String name, LocalDate regDate) {
        System.out.println("Select the breed of the cat:");
        System.out.println("1. " + Breeding.MOGGIE);
        System.out.println("2. " + Breeding.PEDIGREE);
        System.out.print("Enter your choice: ");
        int breedChoice = Integer.parseInt(scanner.nextLine());
        Breeding breeding = breedChoice == 1 ? Breeding.MOGGIE : Breeding.PEDIGREE;

        surgery.makePet(new Cat(name, regDate, breeding));
        System.out.println("Pet Added Successfully!");
    }

    /**
     * Parameters passed as they have already been inputted. here is the menu to select the characteristics if it is an instance of a fish.
     * @param name
     * @param regDate
     */
    private void makeFish(String name, LocalDate regDate) {
        System.out.println("Select the water type for the fish:");
        System.out.println("1. " + WaterType.SALT);
        System.out.println("2. " + WaterType.FRESH);
        System.out.print("Enter your choice: ");
        int waterTypeChoice = Integer.parseInt(scanner.nextLine());
        WaterType waterType = waterTypeChoice == 1 ? WaterType.SALT : WaterType.FRESH;

        surgery.makePet(new Fish(name, regDate, waterType));
        System.out.println("Pet Added Successfully!");
    }
}



