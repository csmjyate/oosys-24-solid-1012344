package main;

import java.time.format.DateTimeFormatter;
import java.util.Scanner;

import ljmu.vets.Pet;
import ljmu.vets.Surgery;

/**
 * The functionality of getting a pets next booking based on it's name.
 * @author jacky
 *
 */
public class GetPetsNextBooking implements Command {
    private final Surgery surgery;
	private final Scanner S = new Scanner(System.in);

	/**
	 * Ensures we are getting the pets next booking from the surgery we want.
	 * @param surgery
	 * @param scanner
	 */
    public GetPetsNextBooking(Surgery surgery, Scanner scanner) {
        this.surgery = surgery;
    }

    /**
     * Executes the main logic of the functionality/
     */
    @Override
    public void execute() {
        while (true) {
            System.out.print("Pet's Name : ");
            String name = S.nextLine();

            Pet pet = surgery.getPetByName(name);
            if (pet != null) {
                System.out.println(pet.getNextBooking().getWhen().format(DateTimeFormatter.ofPattern("dd MMM yy HH:mm")));
                break; 
            } else {
                System.out.println("Pet not found. Please try again.");
            }
        }
    }
}