package main;

import ljmu.vets.Surgery;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class SurgeryMenuTest {
	
	@Test
	public void testExecute() {
	    ByteArrayInputStream in = new ByteArrayInputStream("2\nQ\n".getBytes());
	    System.setIn(in);

	    ByteArrayOutputStream out = new ByteArrayOutputStream();
	    System.setOut(new PrintStream(out));

	    Scanner scanner = new Scanner(System.in);
	    Surgery surgery = new Surgery("SurgeryA"); 
	    CommandFactory commandFactory = new CommandFactory();
	    SurgeryMenu surgeryMenu = new SurgeryMenu(surgery, scanner, commandFactory);

	    surgeryMenu.execute();

	    String expectedOutput = "-- Surgery >> SurgeryA's SURGERY MENU --\n" + 
	                            "2 : Make Pet\n" +
	                            "3 : Get Pet By Name\n" +
	                            "4 : Make Booking\n" +
	                            "5 : Get Pet's Next Booking\n" +
	                            "Q : Quit\n" +
	                            "Pick : ";
	    assertEquals(expectedOutput, out.toString());
	}

}

