package main;

import org.junit.Test;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Scanner;

import static org.junit.Assert.assertTrue;

public class SysTest {

    @Test
    public void testEntryMenu() {
        // Mock user input
        ByteArrayInputStream in = new ByteArrayInputStream("Q\n".getBytes());
        System.setIn(in);

     
        Sys sys = new Sys();

    
        sys.entryMenu();

     
        assertTrue(sys.getEntryMenuExecuted());
    }
}



