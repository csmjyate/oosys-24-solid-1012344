package main;

import ljmu.vets.Surgery;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Unit test for the getting a pets next booking functionality.
 * @author jacky
 *
 */
public class GetPetsNextBookingTest {
    private GetPetsNextBooking getPetsNextBooking;
    private Surgery surgery;
    private ByteArrayOutputStream output;

	/**
	 * Sets up the environment for the test of input of information for the user to
	 * get the pets next booking
	 */
    @Before
    public void setUp() {
        surgery = new Surgery("Test Surgery");
        output = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(output);
        Scanner scanner = new Scanner(System.in);
        getPetsNextBooking = new GetPetsNextBooking(surgery, scanner);
        System.setOut(printStream);
    }

    /**
	 * Sets the modifications made as null so the test doesn't effect the system at
	 * all.
	 */
    @After
    public void tearDown() {
        surgery = null;
        getPetsNextBooking = null;
        output = null;
    }

    /**
     * Executes the unit test for inputting information to prove the functionality works.
     */
    @Test
    public void testExecute_PetFoundWithNextBooking() {
        String input = "GlynH\n";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        getPetsNextBooking.execute();

        String expectedOutput = "11 Nov 25 11:00\n";
        assertTrue(output.toString().contains("11 Nov 25 11:00\n"));
        assertEquals(expectedOutput, output.toString());
    }
}






