package main;

public class Entry {
	/**
	 * Where the program starts. We call to the entry menu in the Sys class.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Sys sys = new Sys();
		sys.entryMenu();
		new Sys().entryMenu();
	}
}
