package main;

import java.util.Scanner;

import ljmu.vets.Surgery;

/**
 * Interface to handle the Pet Commands. Had a try at making a pet Menu for interface segregation principle.
 * @author jacky
 *
 */
public interface PetCommand {
    void execute(Surgery surgery, Scanner scanner);
}

