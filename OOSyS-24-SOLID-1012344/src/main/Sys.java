package main;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import main.CommandFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

import main.*;
import ljmu.vets.*;

public class Sys {

	private final String PATH = "C:\\Users\\jacky\\git\\oosys-24-solid-1012344\\OOSyS-24-SOLID-1012344\\data\\";

	private final Scanner S = new Scanner(System.in);

	private List<Surgery> surgeries = new ArrayList<Surgery>();
	private Surgery surgery;
	private boolean surgeryMenuExecuted = false;
	private boolean entryMenuExecuted = false;
	private final Gson GSON = new Gson();
	Gson gson = new GsonBuilder()
            .registerTypeAdapter(LocalDate.class, new LocalDateAdapter())
            .create();


	
	
/**
 * Inserting some values into the system to make sure the functionalities of the features still work.
 */
	// ToDo : Necessary ?
	public Sys() {
		
		deSerialize();
//		gsonDeserialize();

		surgeries.add(new Surgery("SurgeryA"));
		surgeries.add(new Surgery("SurgeryB"));

		Pet pet1 = new Cat("GlynH", LocalDate.of(2020, 11, 11), Breeding.MOGGIE);
		surgeries.get(0).makePet(pet1);

		Booking booking1 = new Booking("SurgeryA-REF1", pet1, LocalDateTime.of(2022, 9, 9, 9, 33), 30);
		surgeries.get(0).makeBooking(booking1);

		Booking booking2 = new Booking("SurgeryA-REF2", pet1, LocalDateTime.of(2023, 4, 4, 9, 22), 60);
		surgeries.get(0).makeBooking(booking2);

		Booking booking3 = new Booking("SurgeryA-REF3", pet1, LocalDateTime.of(2025, 11, 11, 11, 00), 90);
		surgeries.get(0).makeBooking(booking3);

	}

	/**
	 * Entry menu the user first sees upon launch.
	 */
	public void entryMenu() {
		String choice = "";

		do {
			System.out.println("-- ENTRY MENU --");
			System.out.println("1 : [L]ogOn");
			System.out.println("Q : Quit");
			System.out.print("Pick : ");

			choice = S.nextLine().toUpperCase();

			switch (choice) {
			case "1":
				logOn();
				break;
			case "L": {
				logOn();
				break;
			}
			}

		} while (!choice.equals("Q"));

		entryMenuExecuted = true;
		serialize();
//		gsonSerialize();

		System.out.println("Bye Bye !");
		entryMenuExecuted = true;
		System.exit(0);
	}

	/**
	 * Java deserialisation.
	 */
	private void deSerialize() {
		ObjectInputStream ois;

		try {
			ois = new ObjectInputStream(new FileInputStream(PATH + "surgeries.ser"));

			// NOTE : Erasure Warning !
			this.surgeries = (List<Surgery>) ois.readObject();

			// ToDo : Finally ?
			ois.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * Java serialisation method.
	 */
	private void serialize() {
		ObjectOutputStream oos;

		try {
			oos = new ObjectOutputStream(new FileOutputStream(PATH + "surgeries.ser"));
			oos.writeObject(this.surgeries);

			// ToDo : Finally ?
			oos.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * GSON serialisation method.
	 */
	private void gsonSerialize() {
		try {
			String json = GSON.toJson(surgeries);
			Files.writeString(Path.of(PATH + "surgeries_gson.json"), json);
		} catch (IOException e) {
			System.out.println("Gson serialization failed: " + e.getMessage());
		}
	}

	/**
	 * GSON deserialisation method.
	 */
	private void gsonDeserialize() {
		try {
			String json = Files.readString(Path.of(PATH + "surgeries_gson.json"));
			surgeries = GSON.fromJson(json, new TypeToken<List<Surgery>>() {
			}.getType());
		} catch (IOException e) {
			System.out.println("Failed to read surgeries_gson.json file: " + e.getMessage());
		}
	}

	/**
	 * Log on method to log a user successfully on and execute the display menu.
	 */
	private void logOn() {
		// ToDo: Actually loggingOn :) ?
		this.surgery = this.surgeries.get(0);

		CommandFactory factory = new CommandFactory();
		SurgeryMenu surgeryMenu = new SurgeryMenu(surgery, S, factory);

		// Execute the SurgeryMenu command
		surgeryMenu.execute();
		surgeryMenuExecuted = true;
	}

	/**
	 * Unit testing getters and setters.
	 * @return
	 */
	public boolean getSurgeryMenuExecuted() {
		return surgeryMenuExecuted;
	}

	public boolean getEntryMenuExecuted() {
		return entryMenuExecuted;
	}

}
