package main;

import java.util.Scanner;

import ljmu.vets.Pet;
import ljmu.vets.Surgery;

/**
 * This class gets a pet from a surgery using the pet's name.
 * 
 * @author jacky
 *
 */
public class GetPetByName implements Command {
	private final Surgery surgery;
	private final Scanner S = new Scanner(System.in);

	/**
	 * Gets a pets name based on the surgery we are using.
	 * 
	 * @param surgery
	 * @param scanner
	 */
	public GetPetByName(Surgery surgery, Scanner scanner) {
		this.surgery = surgery;
	}

	/**
	 * To execute the logic of this functionality.
	 */
	@Override
	public void execute() {
		while (true) {
			System.out.print("Pet's Name : ");
			String name = S.nextLine();

			Pet pet = surgery.getPetByName(name);
			if (pet != null) {
				System.out.println(pet.toString());
				break;
			} else {
				System.out.println("Pet not found. Please try again.");
			}
		}
    }
}
