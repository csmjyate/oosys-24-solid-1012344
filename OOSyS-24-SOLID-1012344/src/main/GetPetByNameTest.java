package main;

import ljmu.vets.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.time.LocalDate;
import java.util.Scanner;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Unit test for getting a pet by it's name.
 * @author jacky
 *
 */
public class GetPetByNameTest {
    private GetPetByName getPetByName;
    private Surgery surgery;
    private ByteArrayOutputStream output;

	/**
	 * To set up the environment of finding a pet by its name. Ready for a fake user
	 * inputting the information.
	 */
	@Before
    public void setUp() {
        surgery = new Surgery("Test Surgery");
        output = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(output);
        Scanner scanner = new Scanner(System.in);
        getPetByName = new GetPetByName(surgery, scanner);
        System.setOut(printStream);
    }

	/**
	 * Sets the modifications made as null so the test doesnt effect the system at
	 * all.
	 */
    @After
    public void tearDown() {
        surgery = null;
        getPetByName = null;
        output = null;
    }

    
	/**
	 * Tests a run through of the functionality and if it works the test will
	 * complete.
	 */
	@Test
    public void testExecute_PetFound() {
 
        LocalDate regDate = LocalDate.of(2020, 11, 11);
        Cat cat = new Cat("GlynH", regDate, Breeding.MOGGIE);
        surgery.makePet(cat);

   
        String input = "GlynH\n";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);


        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(outputStream);
        System.setOut(printStream);

  
        Scanner scanner = new Scanner(System.in);


        getPetByName = new GetPetByName(surgery, scanner);


        getPetByName.execute();

        assertTrue(outputStream.toString().contains("Cat >> GlynH 11-Nov-20 MOGGIE"));
    }
}
