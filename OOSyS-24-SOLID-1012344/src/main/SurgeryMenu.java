package main;

import java.util.Scanner;
import ljmu.vets.Surgery;

/**
 * Class for displaying the surgery menu and a users input will commit a command from the command factory.
 * @author jacky
 *
 */
public class SurgeryMenu implements Command {
    private final Scanner scanner;
    private final Surgery surgery;
    private final CommandFactory commandFactory;

    /**
     * Assigns a scanner to the menu, and ensures the menu belongs to the surgery we want.
     * @param surgery
     * @param scanner
     * @param commandFactory
     */
    public SurgeryMenu(Surgery surgery,Scanner scanner, CommandFactory commandFactory) {
    	 this.surgery = surgery;
        this.scanner = scanner;
        this.commandFactory = commandFactory;
        
       
    }
/**
 * Menu
 */
	public void displayMenu() {
		System.out.println("-- " + surgery.toString() + "'s SURGERY MENU --");
		System.out.println("2 : Make Pet");
		System.out.println("3 : Get Pet By Name");
		System.out.println("4 : Make Booking");
		System.out.println("5 : Get Pet's Next Booking");
		System.out.println("Q : Quit");
		System.out.print("Pick : ");
	}

	/**
	 * Executes the users choice and depending on the user's choice the command
	 * number will be executed.
	 */
	public void execute() {
        String choice;
        do {
            displayMenu();
            choice = scanner.nextLine().toUpperCase();
            if (!choice.equals("Q")) {
                Command command = commandFactory.getCommand(choice, surgery, scanner);
                if (command != null) {
                    command.execute();
                } else {
                    System.out.println("Invalid choice.");
                }
            }
        } while (!choice.equals("Q"));
    }
}



